﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Entities;
using static SDL2.SDL;

namespace XOGame.Extensions
{
    public static class SDLRectExtension
    {
        public static bool Intersects(this SDL_Rect rect, Point position)
        {
            return (position.X >= rect.x && position.X <= (rect.x + rect.w)) &&
                   (position.Y >= rect.y && position.Y <= (rect.y + rect.h));
        }
    }
}
