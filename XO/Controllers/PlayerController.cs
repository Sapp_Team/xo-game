﻿using XOGame.Entities;
using XOGame.InputStructs;

namespace XOGame.Controllers
{
    public class PlayerController : IController
    {
        public Point GetPosition()
        {
            Point point = new Point();

            point.X = Input.MouseState.PositionX;
            point.Y = Input.MouseState.PositionY;
            
            
            return point;
        }
    }
}