﻿using System;
using System.Collections.Generic;
using System.Text;

using XOGame.Entities;

namespace XOGame.Controllers
{
    public class AIController: IController
    {
        private Random rand = new Random(Guid.NewGuid().GetHashCode());

        public Point GetPosition()
        {
            Point point;
            point.X = rand.Next(300);
            point.Y = rand.Next(300);
            return point;
        }
    }
}
