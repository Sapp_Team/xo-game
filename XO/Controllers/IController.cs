﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Entities;

namespace XOGame.Controllers
{
    public interface IController
    {
        Point GetPosition();
    }
}
