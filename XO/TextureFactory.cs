﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XOGame
{
    public static class TextureFactory
    {
        private static Dictionary<XOGame.Entities.PlayerID, Texture> _textureDict = new Dictionary<XOGame.Entities.PlayerID, Texture>();

        public static void InitFactory(IntPtr rendererHandle)
        {
            string cellPath = @"Content\Textures\cell.png";
            string cellXPath = @"Content\Textures\X.png";
            string cellOPath = @"Content\Textures\O.png";

            Texture tmpTexture = new Texture();
            tmpTexture.LoadTextureFromPNG(cellPath, rendererHandle);
            _textureDict.Add(XOGame.Entities.PlayerID.Nobody, new Texture(tmpTexture));

            //Texture tmpXTexture = new Texture();
            tmpTexture.LoadTextureFromPNG(cellXPath, rendererHandle);
            _textureDict.Add(XOGame.Entities.PlayerID.Player1, new Texture(tmpTexture));

            //Texture tmpOTexture = new Texture();
            tmpTexture.LoadTextureFromPNG(cellOPath, rendererHandle);
            _textureDict.Add(XOGame.Entities.PlayerID.Player2, new Texture(tmpTexture));
        }

        public static Texture GetTexture(XOGame.Entities.PlayerID playerID)
        {
            //if (_textureDict[playerID] != null)
                return _textureDict[playerID];
            //else
            //    return null;
        }
    }
}
