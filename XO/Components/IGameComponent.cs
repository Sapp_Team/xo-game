﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XOGame.Components
{
    public interface IGameComponent
    {
        void Initialize();
        void Update(float deltaTime);
        void Render(Renderer renderer);
    }
}
