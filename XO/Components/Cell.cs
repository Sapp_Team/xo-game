﻿using System;
using System.Collections.Generic;
using System.Text;

using static SDL2.SDL;
using XOGame.Entities;
using XOGame.Components;
using XOGame.Extensions;

namespace XOGame.Components
{
    public enum CellState
    {
        Empty = 0,
        Fill
    }

    public class Cell: IGameComponent, IDisposable
    {
        private PlayerID _playerID;
        private CellState _cellState;
        private Sprite _sprite;

        private bool _disposed;

        public PlayerID PlayerID { get => _playerID; set => _playerID = value; }
        public CellState State { get => _cellState; set => _cellState = value; }
        public SDL_Rect Bound { get => _sprite.Bound; }

        public Cell(Sprite sprite)
        {
            _sprite = sprite;
            _cellState = CellState.Empty;
            _playerID = PlayerID.Nobody;
            _disposed = false;
        }

        public void Initialize()
        {

        }

        public void Update(float deltaTime)
        {
            
        }

        public void Render(Renderer render)
        {
            render.RenderSprite(_sprite);
        }

        public void Dispose()
        {
            if(!_disposed)
            {
                _sprite.Dispose();
                _disposed = true;
            }
        }

        internal void Reset()
        {
            _cellState = CellState.Empty;
            _playerID = PlayerID.Nobody;
            _sprite.Texture = TextureFactory.GetTexture(PlayerID.Nobody);
        }

        internal bool IsClickedIn(Point position)
        {
            return Bound.Intersects(position);
        }

        internal void MakeFilledBy(PlayerID playerID)
        {
            State = CellState.Fill;
            PlayerID = playerID;
            _sprite.Texture = TextureFactory.GetTexture(playerID);
        }
    }
}
