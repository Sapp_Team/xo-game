﻿using System;
using System.Collections.Generic;
using XOGame.Entities;
using static SDL2.SDL;

namespace XOGame
{
    public static class MessageBox
    {
        public enum DialogResult
        {
            No = 0,
            Yes = 1
        }

        public static void ShowInfo(string title, string message)
        {
            SDL_MessageBoxButtonData[] buttons = new SDL_MessageBoxButtonData[1]
            { new SDL_MessageBoxButtonData()
                { buttonid = 0, flags = SDL_MessageBoxButtonFlags.SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, text = "I see"}
            };

            SDL_MessageBoxColorScheme colorScheme = new SDL_MessageBoxColorScheme();
            colorScheme.colors = new SDL_MessageBoxColor[5];

            SDL_MessageBoxData msgboxdData = new SDL_MessageBoxData()
            {
                flags = SDL_MessageBoxFlags.SDL_MESSAGEBOX_INFORMATION,
                window = IntPtr.Zero,
                title = title,
                message = message,
                numbuttons = 1,
                buttons = buttons,
                colorScheme = colorScheme
            };

            int buttonID;
            SDL_ShowMessageBox(ref msgboxdData, out buttonID);
        }

        public static DialogResult Show(string title, string message)
        {
            //Answer gettingAnswer;

            SDL_MessageBoxButtonData[] buttons = new SDL_MessageBoxButtonData[2]
            {
                new SDL_MessageBoxButtonData()
                {
                    buttonid = 0,
                    flags = SDL_MessageBoxButtonFlags.SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT,
                    text = "No"
                },

                new SDL_MessageBoxButtonData()
                {
                    buttonid = 1,
                    flags = SDL_MessageBoxButtonFlags.SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT,
                    text = "Yes"
                }
            };

            SDL_MessageBoxColorScheme colorScheme = new SDL_MessageBoxColorScheme();
            colorScheme.colors = new SDL_MessageBoxColor[5];

            SDL_MessageBoxData msgboxdData = new SDL_MessageBoxData()
            {
                flags = SDL_MessageBoxFlags.SDL_MESSAGEBOX_INFORMATION,
                window = IntPtr.Zero,
                title = title,
                message = message,
                numbuttons = buttons.Length,
                buttons = buttons,
                colorScheme = colorScheme
            };

            int buttonID;
            SDL_ShowMessageBox(ref msgboxdData, out buttonID);

            return (DialogResult)buttonID;
        }
    }

    
}
