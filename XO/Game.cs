using System;
using System.Collections.Generic;
using SDL2;
using XOGame.Scenes;
using static SDL2.SDL;
using static SDL2.SDL.SDL_WindowFlags;

namespace XOGame
{
    
    public class Game : GameWindow, IDisposable
    {
        private BaseScene _sampleScene;

        public Game(
            SDL_WindowFlags windowFlag = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE, 
            uint systemFlag = SDL_INIT_EVERYTHING, 
            int width = 400, 
            int height = 400, 
            string title = "Window") 
            : base(windowFlag, systemFlag, width, height, title)
        {
            _sampleScene = new MainScene("Sample Scene", Renderer.Region);
        }

        public override void Initialize()
        {
            TextureFactory.InitFactory(Renderer.Handle);
            _sampleScene.Initialize();
        }

        public override void Update(float deltaTime)
        {
            if (Input.Keyboard.Key == SDL.SDL_Scancode.SDL_SCANCODE_ESCAPE)
                Exit();
            _sampleScene.Update(deltaTime);
        }

        public override void Render(Renderer renderer)
        {
            renderer.Clear();
            _sampleScene.Render(renderer);
            renderer.Update();
        }

        #region Trash

        //public enum GameState
        //{
        //    Begin = 0,
        //    Player1,
        //    Player2,
        //    Move,
        //    End
        //}

        //private bool _running;
        //private bool _disposed;
        //private string _textureFolderPath = @"Content\Textures\";

        //private Window _window = new Window();
        //private Renderer _renderer = new Renderer();

        //private Texture _field = new Texture();
        //private Texture _xCell = new Texture();
        //private Texture _oCell = new Texture();

        //// always equal in number of elements 
        //private List<GameState> _playersQueue = new List<GameState>();
        //private List<SDL_Rect> _drawedCellsPositions = new List<SDL_Rect>();

        //private int _widthTextureCell;
        //private int _heightTextureCell;

        //private Random rand = new Random(Guid.NewGuid().GetHashCode());

        //GameState[] _fieldsState = new GameState[9];
        //GameState _currentState;

        //public Game() : base()
        //{
        //    _running = true;
        //    _currentState = GameState.Begin;
        //    _disposed = false;
        //    _widthTextureCell = 0;
        //    _heightTextureCell = 0;

        //    for (int i = 0; i < 9; i++)
        //    {
        //        _fieldsState[i] = GameState.Move;
        //    }
        //}

        //public void Start()
        //{
        //    _window.Initialize(SDL_INIT_EVERYTHING);
        //    _window.OpenWindow(300, 300, "XO", SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        //    _widthTextureCell = (300 / 3) - 1;
        //    _heightTextureCell = (300 / 3) - 1;

        //    _renderer.Initialize(_window.Handle);
        //    LoadContent();
        //}

        //private void LoadContent()
        //{
        //    _field.LoadTexture(_textureFolderPath + "field.bmp", _renderer.Handle);
        //    _xCell.LoadTexture(_textureFolderPath + "X.bmp", _renderer.Handle);
        //    _oCell.LoadTexture(_textureFolderPath + "O.bmp", _renderer.Handle);
        //}

        //public void Run()
        //{
        //    Start();

        //    _currentState = GameState.Player1;
        //    _renderer.RenderTexture(_field.Handle);

        //    while(_running)
        //    {
        //        while (SDL_PollEvent(out SDL_Event pulledEvents) == 1)
        //            OnEvent(ref pulledEvents);

        //        if (_currentState == GameState.Player2)
        //            ActionP2();

        //        _renderer.Clear();
        //        _renderer.RenderTexture(_field.Handle);

        //        for(int i = 0; i < _playersQueue.Count; i++)
        //        {
        //            SDL_Rect rectTmp = _drawedCellsPositions[i];

        //            if(_playersQueue[i] == GameState.Player1)
        //            {
        //                _renderer.RenderTexture(_xCell.Handle, ref rectTmp);
        //            }
        //            else if(_playersQueue[i] == GameState.Player2)
        //            {
        //                _renderer.RenderTexture(_oCell.Handle, ref rectTmp);

        //            }
        //        }

        //        _renderer.Update();
        //        GameState currentState;
        //        if(!ProcessFields(out currentState))
        //        {
        //            if(currentState == GameState.Player1)
        //            {
        //                Console.WriteLine("Win is PLAYER 1");
        //                _running = false;
        //            }
        //            else if(currentState == GameState.Player2)
        //            {
        //                Console.WriteLine("Win is PLAYER 2");
        //                _running = false;
        //            }
        //        }

        //        if(_currentState == GameState.End)
        //        {
        //            Console.WriteLine("Win is PLAYER 1");
        //            _running = false;
        //        }
        //    }

        //    //SDL_Delay(5000);
        //    Quit();
        //}


        //private bool ProcessFields(out GameState winner)
        //{
        //    if((_fieldsState[0] == GameState.Player1 && _fieldsState[4] == GameState.Player1 && _fieldsState[8] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    else if((_fieldsState[0] == GameState.Player2 && _fieldsState[4] == GameState.Player2 && _fieldsState[8] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }

        //    if((_fieldsState[2] == GameState.Player1 && _fieldsState[4] == GameState.Player1 && _fieldsState[6] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    else if((_fieldsState[2] == GameState.Player2 && _fieldsState[4] == GameState.Player2 && _fieldsState[6] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }

        //    if((_fieldsState[0] == GameState.Player1 && _fieldsState[1] == GameState.Player1 && _fieldsState[2] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[0] == GameState.Player2 && _fieldsState[1] == GameState.Player2 && _fieldsState[2] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[3] == GameState.Player1 && _fieldsState[4] == GameState.Player1 && _fieldsState[5] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[3] == GameState.Player2 && _fieldsState[4] == GameState.Player2 && _fieldsState[5] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[6] == GameState.Player1 && _fieldsState[7] == GameState.Player1 && _fieldsState[8] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[6] == GameState.Player2 && _fieldsState[7] == GameState.Player2 && _fieldsState[8] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[0] == GameState.Player1 && _fieldsState[3] == GameState.Player1 && _fieldsState[6] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[0] == GameState.Player2 && _fieldsState[3] == GameState.Player2 && _fieldsState[6] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[1] == GameState.Player1 && _fieldsState[4] == GameState.Player1 && _fieldsState[7] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[1] == GameState.Player2 && _fieldsState[4] == GameState.Player2 && _fieldsState[7] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[2] == GameState.Player1 && _fieldsState[5] == GameState.Player1 && _fieldsState[8] == GameState.Player1))
        //    {
        //        winner = GameState.Player1;
        //        _currentState = GameState.End;
        //        return false;
        //    }
        //    if((_fieldsState[2] == GameState.Player2 && _fieldsState[5] == GameState.Player2 && _fieldsState[8] == GameState.Player2))
        //    {
        //        winner = GameState.Player2;
        //        _currentState = GameState.End;
        //        return false;
        //    }

        //    winner = GameState.Move;
        //    return true;
        //}

        //private void ActionP1(int x, int y)
        //{
        //    if(_currentState != GameState.End)
        //    {
        //        SDL_Rect rect = new SDL_Rect() {x = x+1, y = y+1, w = _widthTextureCell, h = _heightTextureCell};
        //        _xCell.DestRect = rect;
        //        _drawedCellsPositions.Add(rect);
        //        _playersQueue.Add(GameState.Player1);
        //        //_renderer.RenderTexture(_xCell.Handle, ref rect);

        //        _currentState = GameState.Player2;
        //    }
        //}

        //private void ActionP2()
        //{
        //    if(_currentState != GameState.End)
        //    {
        //        SDL_Rect rect;

        //        int x = rand.Next(0, _window.Width);
        //        int y = rand.Next(0, _window.Height);
        //        int newX = 0; int newY = 0;

        //        if(ProcessPositionOnFields(x, y, out newX, out newY))
        //        {
        //            rect.x = newX+1;
        //            rect.y = newY+1;
        //            rect.w = _widthTextureCell;
        //            rect.h = _heightTextureCell;

        //            _oCell.DestRect = rect;
        //            _drawedCellsPositions.Add(rect);
        //            _playersQueue.Add(GameState.Player2);

        //            //_renderer.RenderTexture(_oCell.Handle, ref rect);

        //            _currentState = GameState.Player1;
        //        }
        //    }
        //}

        //private bool ProcessPositionOnFields(int xSrc, int ySrc, out int xDest, out int yDest)
        //{
        //    bool result = false;
        //    int x = 0, y = 0;
        //    int widthField = _window.Width / 3;
        //    int heightField = _window.Height / 3;

        //    if(ySrc <= heightField)
        //    {
        //        if(xSrc <= widthField && _fieldsState[0] == GameState.Move)
        //        {
        //            _fieldsState[0] = _currentState;
        //            x = 0;
        //            y = 0;
        //            result = true;
        //        }
        //        if(xSrc <= widthField*2 && xSrc >= widthField && _fieldsState[1] == GameState.Move)
        //        {
        //            _fieldsState[1] = _currentState;
        //            x = widthField;
        //            y = 0;
        //            result = true;
        //        }
        //        if(xSrc >= widthField*2 && _fieldsState[2] == GameState.Move)
        //        {
        //            _fieldsState[2] = _currentState;
        //            x = widthField*2;
        //            y = 0;
        //            result = true;
        //        }
        //    }
        //    else if(ySrc >= heightField && ySrc <= heightField*2)
        //    {
        //        if(xSrc <= widthField && _fieldsState[3] == GameState.Move)
        //        {
        //            _fieldsState[3] = _currentState;
        //            x = 0;
        //            y = heightField;
        //            result = true;
        //        }
        //        if(xSrc <= widthField*2 && xSrc >= widthField && _fieldsState[4] == GameState.Move)
        //        {
        //            _fieldsState[4] = _currentState;
        //            x = widthField;
        //            y = heightField;
        //            result = true;
        //        }
        //        if(xSrc >= widthField*2 && _fieldsState[5] == GameState.Move)
        //        {
        //            _fieldsState[5] = _currentState;
        //            x = widthField*2;
        //            y = heightField;
        //            result = true;
        //        }
        //    }
        //    else if(ySrc >= heightField*2)
        //    {
        //        if(xSrc <= widthField && _fieldsState[6] == GameState.Move)
        //        {
        //            _fieldsState[6] = _currentState;
        //            x = 0;
        //            y = heightField*2;
        //            result = true;
        //        }
        //        if(xSrc <= widthField*2 && xSrc >= widthField && _fieldsState[7] == GameState.Move)
        //        {
        //            _fieldsState[7] = _currentState;
        //            x = widthField;
        //            y = heightField*2;
        //            result = true;
        //        }
        //        if(xSrc >= widthField*2 && _fieldsState[8] == GameState.Move)
        //        {
        //            _fieldsState[8] = _currentState;
        //            x = widthField*2;
        //            y = heightField*2;
        //            result = true;
        //        }
        //    }


        //    xDest = x; yDest = y;

        //    return result;
        //}

        //private void RecalculatePositionsForCells(int diffWidth, int diffHeight)
        //{
        //    int oldWidthField = _window.OldWidth / 3;
        //    int oldHeightField = _window.OldHeight / 3;

        //    for(int i = 0; i < _drawedCellsPositions.Count; i++)
        //    {
        //        SDL_Rect rectTmp = new SDL_Rect() { x = 1, y = 1, w = 0, h = 0 };
        //        if(_drawedCellsPositions[i].x != 1)
        //        {
        //            if(_drawedCellsPositions[i].x >= (oldWidthField*2)+1)
        //                rectTmp.x = _drawedCellsPositions[i].x + (diffWidth*2) + 1;
        //            else
        //                rectTmp.x = _drawedCellsPositions[i].x + diffWidth + 1;
        //        }
        //        if (_drawedCellsPositions[i].y != 1)
        //        {
        //            if(_drawedCellsPositions[i].y >= (oldHeightField*2)+1)
        //                rectTmp.y = _drawedCellsPositions[i].y + (diffHeight*2) + 1;
        //            else
        //                rectTmp.y = _drawedCellsPositions[i].y + diffHeight + 1;

        //        }

        //        rectTmp.w = _widthTextureCell;
        //        rectTmp.h = _heightTextureCell;

        //        Console.WriteLine($"Old: {_drawedCellsPositions[i].x}, {_drawedCellsPositions[i].y}, {_drawedCellsPositions[i].w}, {_drawedCellsPositions[i].h}");
        //        Console.WriteLine($"New: {rectTmp.x}, {rectTmp.y}, {rectTmp.w}, {rectTmp.h}");
        //        Console.WriteLine("===");

        //        _drawedCellsPositions[i] = rectTmp;
        //    }
        //}

        //// Overload
        //protected override void OnExit()
        //{
        //    _running = false;
        //    Console.WriteLine("Exit");
        //}

        //protected override void OnButtonMouseLeftDown(int x, int y)
        //{
        //    if(_currentState == GameState.Player1)
        //    {
        //        int newX = 0, newY = 0;
        //        if(ProcessPositionOnFields(x, y, out newX, out newY))
        //        {
        //            ActionP1(newX, newY);
        //        }
        //    }
        //}

        //protected override void OnWindowResized(int newWidth, int newHeight)
        //{
        //    int diffWidth = (newWidth/3) - (_window.Width / 3);
        //    int diffheight = (newHeight/3) - (_window.Height / 3);

        //    Console.WriteLine($"Current: {_window.Width}, {_window.Height}");

        //    _window.Width = newWidth;
        //    _window.Height = newHeight;

        //    _widthTextureCell = (_window.Width / 3) - 1;
        //    _heightTextureCell = (_window.Height / 3) - 1;
        //    RecalculatePositionsForCells(diffWidth, diffheight);

        //    Console.WriteLine($"New: {newWidth}, {newHeight}");
        //    Console.WriteLine($"Diff: {diffWidth}, {diffheight}");
        //    Console.WriteLine($"New size cells: {_widthTextureCell}, {_heightTextureCell}");
        //    Console.WriteLine("-----------");

        //}

        //public override void Dispose()
        //{
        //    if(!_disposed)
        //    {
        //        _field.Dispose();
        //        _xCell.Dispose();
        //        _oCell.Dispose();
        //        _renderer.Dispose();
        //        _window.Dispose();
        //        _disposed = true;
        //    }
        //}

        //public override void Initialize()
        //{
        //    throw new NotImplementedException();
        //}

        //public override void Update(float deltaTime)
        //{
        //    throw new NotImplementedException();
        //}

        //public override void Render(float deltaTime)
        //{
        //    throw new NotImplementedException();
        //}
        #endregion
    }
}