using System;

using static SDL2.SDL;
using image = SDL2.SDL_image;

namespace XOGame
{
    public class Texture: ICloneable, IDisposable
    {
        private bool _disposed;
        private IntPtr _texture;

        public IntPtr Handle { get { return _texture; } }

        public Texture()
        {
            _disposed = false;
            _texture = IntPtr.Zero;
        }

        public Texture(Texture texture)
        {
            _texture = texture.Handle;
            _disposed = texture._disposed;
        }

        public void LoadTextureFromBMP(string pathToFile, IntPtr renderer)
        {
            if(_texture != IntPtr.Zero)
                SDL_DestroyTexture(_texture);

            var surface = SDL_LoadBMP(pathToFile);
            if(surface == IntPtr.Zero)
                throw new Exception($"BMP was not loaded. Error: {GetError()}");

            _texture = SDL_CreateTextureFromSurface(renderer, surface);
            SDL_FreeSurface(surface);
            if(_texture == IntPtr.Zero)
                throw new Exception($"Texture was not create. Error: {GetError()}");
        }

        public void LoadTextureFromPNG(string pathToFile, IntPtr renderer)
        {
            int initted = image.IMG_Init(image.IMG_InitFlags.IMG_INIT_PNG);
            if((initted & (int)image.IMG_InitFlags.IMG_INIT_PNG) != (int)image.IMG_InitFlags.IMG_INIT_PNG)
                throw new Exception($"SDL_Image was not initialized (PNG). Error: {GetError()}");

            var surface = image.IMG_Load(pathToFile);
            if(surface == IntPtr.Zero)
                throw new Exception($"PNG was not loaded. Error: {GetError()}");

            _texture = SDL_CreateTextureFromSurface(renderer, surface);
            SDL_FreeSurface(surface);
            if(_texture == IntPtr.Zero)
                throw new Exception($"Texture was not create. Error: {GetError()}");

            image.IMG_Quit();
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                SDL_DestroyTexture(_texture);
                _disposed = true;
            }
        }

        public object Clone()
        {
            return new Texture() { _disposed = this._disposed, _texture = this._texture };
        }
    }
}