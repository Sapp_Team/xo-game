using System;
using XOGame.Scenes;
using static SDL2.SDL;
using static SDL2.SDL.SDL_RendererFlags;
using Image = SDL2.SDL_image;

namespace XOGame
{
    public class Renderer: IDisposable
    {
        private bool _disposed;
        private IntPtr _handle;
        private IntPtr _windowHandle;

        private RenderRegion _region;
        private readonly int DefaultWidth = 300;
        private readonly int DefaultHeight = 300;

        public IntPtr Handle { get { return _handle; } }

        public RenderRegion Region { get => _region; set => _region = value; }

        public Renderer(IntPtr windowHandle)
        {
            _disposed = false;
            _windowHandle = windowHandle;
            _region = new RenderRegion(DefaultWidth, DefaultHeight);
        }

        public void Initialize()
        {
            _handle = IntPtr.Zero;
            _handle = SDL_CreateRenderer(
                            _windowHandle, 
                            -1, 
                            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            
            if(_handle == IntPtr.Zero)
                throw new ArgumentException($"Renderer was not created. Error: {GetError()}");

            if(SDL_RenderSetLogicalSize(_handle, _region.Width, _region.Height) < 0)
                throw new ArgumentException($"Cant set logicale size for renderer. Error: {GetError()}");
        }

        public void RenderTexture(IntPtr texture, SDL_Rect dstrect)
        {
            SDL_RenderCopy(_handle, texture, IntPtr.Zero, ref dstrect);
        }

        public void RenderTexture(Texture texture, SDL_Rect dstrect)
        {
            SDL_RenderCopy(_handle, texture.Handle, IntPtr.Zero, ref dstrect);
        }

        public void RenderTexture(IntPtr texture)
        {
            SDL_RenderCopy(_handle, texture, IntPtr.Zero, IntPtr.Zero);
        }

        public void RenderTexture(Texture texture)
        {
            SDL_RenderCopy(_handle, texture.Handle, IntPtr.Zero, IntPtr.Zero);
        }

        public void RenderSprite(Sprite sprite)
        {
            SDL_Rect rect = sprite.Bound;
            IntPtr texture = sprite.Texture.Handle;
            SDL_RenderCopy(_handle, texture, IntPtr.Zero, ref rect);
        }

        public void Update()
        {
            SDL_RenderPresent(_handle);
        }

        public void Clear()
        {
            SDL_RenderClear(_handle);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                SDL_DestroyRenderer(_handle);
                _disposed = true;
            }
        }
    }
}