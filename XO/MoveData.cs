﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Entities;

namespace XOGame
{
    internal class MoveData
    {
        private PlayerID _playerID;
        private bool _succesfulMove;
        private Point _cellIndex;

        public PlayerID PlayerID { get => _playerID ; set => _playerID = value; }
        public bool SuccesfulMove { get => _succesfulMove; set => _succesfulMove = value; }
        public Point CellIndex { get => _cellIndex; set => _cellIndex = value; }

        public MoveData()
        {
            _playerID = PlayerID.Nobody;
            _succesfulMove = false;
            _cellIndex = new Point(0, 0);
        }
    }
}
