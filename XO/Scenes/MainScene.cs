﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using XOGame.Entities;
using XOGame.Controllers;
using XOGame.InputStructs;

namespace XOGame.Scenes
{
    public class MainScene : BaseScene, IDisposable
    {
        private Player _player;
        private Player _AIPlayer;

        private Field _field;
        private const int FieldSize = 3;

        private MoveController _moveController;

        private bool _disposed;

        public MainScene(string name, RenderRegion renderRegion) : base(name)
        {
            _player = new Player(PlayerID.Player1, new PlayerController());
            _AIPlayer = new Player(PlayerID.Player2, new AIController());

            _field = new Field(FieldSize, renderRegion);

            Queue<Action<float>> actions = new Queue<Action<float>>();
            actions.Enqueue(_player.Update);
            actions.Enqueue(_AIPlayer.Update);

            _moveController = new MoveController(actions);

            _player.Moved += _field.OnPlayerMoved;
            _AIPlayer.Moved += _field.OnPlayerMoved;

            _field.MoveSucces += _moveController.OnDequeuePlayer;
            _field.GameOver += _moveController.OnClearQueue;
            _field.RestartGame += _moveController.OnReinitialize;

            _disposed = false;
        }

        public override void Initialize()
        {
            _moveController.Initialize();
            _field.Initialize();
        }

        public override void Update(float deltaTime)
        {
            _moveController.Update(deltaTime);
            _field.Update(deltaTime);
        }

        public override void Render(Renderer renderer)
        {
            _field.Render(renderer);
        }

        public void Dispose()
        {
            if(!_disposed)
            {
                _player.Moved -= _field.OnPlayerMoved;
                _AIPlayer.Moved -= _field.OnPlayerMoved;

                _field.MoveSucces -= _moveController.OnDequeuePlayer;
                _field.GameOver -= _moveController.OnClearQueue;
                _field.RestartGame -= _moveController.OnReinitialize;
                _disposed = true;
            }
        }
    }
}
