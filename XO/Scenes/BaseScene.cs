﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Components;

namespace XOGame.Scenes
{
    public abstract class BaseScene : IGameComponent
    {
        public string Name { get; set; }

        protected BaseScene(string name)
        {
            Name = name;
        }

        public abstract void Initialize();
        public abstract void Render(Renderer renderer);
        public abstract void Update(float deltaTime);
    }
}
