﻿using System;
using System.Collections.Generic;
using System.Text;

using XOGame.Controllers;

namespace XOGame.Entities
{
    public enum PlayerID
    {
        Nobody = 0,
        Player1,
        Player2
    }

    public class Player : BaseEntity
    {
        private PlayerID _playerID;
        private IController _controller;

        public event EventHandler<MoveInfoEventArgs> Moved = delegate { };

        public Player(PlayerID playerID, IController controller)
        {
            _playerID = playerID;
            _controller = controller;
        }

        public override void Initialize()
        {
            
        }

        public override void Render(Renderer render)
        {
            
        }

        public override void Update(float deltaTime)
        {
            var MouseState = Input.MouseState;
            if(MouseState.Button == InputStructs.MouseButton.Left && MouseState.ButtonState == InputStructs.MouseButtonState.Pressed)
            { 
                var clickPosition = _controller.GetPosition();
                Moved(this, new MoveInfoEventArgs(clickPosition, _playerID));
            }
        }
    }

    public class MoveInfoEventArgs : EventArgs
    {
        public Point Position { get; }
        public PlayerID PlayerID { get; }

        public MoveInfoEventArgs(Point pos, PlayerID playerID)
        {
            Position = pos;
            PlayerID = playerID;
        }
    }
}
