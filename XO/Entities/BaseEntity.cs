﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Components;

namespace XOGame.Entities
{
    public abstract class BaseEntity : IGameComponent
    {
        public Guid ID { get; set; }
        public Guid ParentID { get; set; }

        public abstract void Initialize();
        public abstract void Update(float deltaTime);
        public abstract void Render(Renderer render);
    }
}

