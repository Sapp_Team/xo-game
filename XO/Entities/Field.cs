﻿using XOGame.Components;
using XOGame.Entities;
using System.Collections.Generic;
using static SDL2.SDL;
using System;
using XOGame.Extensions;

namespace XOGame.Scenes
{
    internal class Field : BaseEntity
    {
        private int _size;

        private RenderRegion _renderRegion;
        private Cell[,] _cells;

        public event EventHandler MoveSucces = delegate { };
        public event EventHandler GameOver = delegate { };
        public event EventHandler RestartGame = delegate { };

        public Field(int size, RenderRegion renderRegion)
        {
            _size = size;

            _renderRegion = renderRegion;
            _cells = new Cell[_size, _size];

        }

        public override void Initialize()
        {
            int cellWidth = _renderRegion.Width / _size;
            int cellHeight = _renderRegion.Height / _size;

            var emptyTexture = TextureFactory.GetTexture(PlayerID.Nobody);
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    SDL_Rect bound = new SDL_Rect();
                    bound.x = i * cellWidth;
                    bound.y = j * cellHeight;
                    bound.h = cellHeight;
                    bound.w = cellWidth;

                    var sprite = new Sprite(emptyTexture, bound);
                    _cells[i,j] = new Cell(sprite);
                }
            }
            
        }

        public override void Update(float deltaTime)
        {

        }

        public override void Render(Renderer render)
        {
            for (int i = 0; i < _size; i++)
            {
                for(int j = 0; j < _size; j++)
                {
                    _cells[i,j].Render(render);
                }
            }
        }

        public void OnPlayerMoved(object sender, MoveInfoEventArgs e)
        {
            var clickedCell = GetClickedCell(e.Position, out Point cellIndex);

            if (clickedCell != null && clickedCell.State == CellState.Empty)
            {
                clickedCell.MakeFilledBy(e.PlayerID);

                if (!CheckCombinations(cellIndex, e.PlayerID))
                {
                    MoveSucces(this, new EventArgs());
                    CheckCellsState();
                }
            }

            #region old
            //CheckCollision(e.Position, out Point indexCell);
            //int i = indexCell.X;
            //int j = indexCell.Y;
            //if (_cells[i, j].CellState == CellState.Empty)
            //{
            //    var currentCell = _cells[i, j];

            //    currentCell.CellState = CellState.Fill;
            //    currentCell.PlayerID = e.PlayerID;
            //    currentCell.Sprite.Texture = TextureFactory.GetTexture(e.PlayerID);

            //    /// TODO thinking on this
            //    _lastIndexCell = indexCell;
            //    _lastMoveWasSucces = true;
            //    _lastMoveMakePlayer = e.PlayerID;
            //    /// TODO end

            //    MoveSucces(this, new EventArgs());
            //}
            #endregion
        }

        private Cell GetClickedCell(Point position, out Point cellIndex)
        {
            cellIndex = new Point(-1, -1);
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    var curCell = _cells[i, j];

                    if (curCell.IsClickedIn(position))
                    {
                        cellIndex.X = i;
                        cellIndex.Y = j;
                        return curCell;
                    }
                }
            }

            return null;
        }

        private bool CheckCombinations(Point lastIndexCell, PlayerID playerID)
        {
            int i = 0;
            int idOnLine = 0;

            if (lastIndexCell.X == lastIndexCell.Y)
            {
                for (int j = 0; j < _size; j++, i++)
                {
                    Cell cell = _cells[i, j];
                    if (cell.PlayerID == playerID)
                        idOnLine++;
                }
                if (idOnLine == 3)
                {
                    MessageProcess(playerID);
                    return true;
                }
            }

            i = _size - 1; idOnLine = 0;
            for (int j = 0; j < _size; j++, i--)
            {
                Cell cell = _cells[i, j];
                if (cell.PlayerID == playerID)
                    idOnLine++;
            }
            if (idOnLine == 3)
            {
                MessageProcess(playerID);
                return true;
            }

            i = lastIndexCell.X; idOnLine = 0;
            for (int j = 0; j < _size; j++)
            {
                Cell cell = _cells[i, j];
                if (cell.PlayerID == playerID)
                    idOnLine++;
            }
            if (idOnLine == 3)
            {
                MessageProcess(playerID);
                return true;
            }

            i = lastIndexCell.Y; idOnLine = 0;
            for (int j = 0; j < _size; j++)
            {
                Cell cell = _cells[j, i];
                if (cell.PlayerID == playerID)
                    idOnLine++;
            }
            if (idOnLine == 3)
            {
                MessageProcess(playerID);
                return true;
            }
            return false;
        }

        private void CheckCellsState()
        {
            int countFilled = 0;
            for (int i = 0; i < _size; i++)
            {
                for(int j = 0; j < _size; j++)
                {
                    if (_cells[i, j].State != CellState.Empty)
                        countFilled++;
                }
            }

            if (countFilled == _size * _size)
            {
                MessageProcess(PlayerID.Nobody);
            }
        }

        private void MessageProcess(PlayerID playerID)
        {
            string message;

            if (playerID == PlayerID.Nobody)
                message = "Никто не выиграл. \nХотите начать заново?";
            else
                message = "Выиграл игрок " + playerID.ToString() + ".\nХотите начать заново?";

            var result = MessageBox.Show("Результат игры", message);

            if (result == MessageBox.DialogResult.Yes)
            {
                ResetCells();
                RestartGame(this, new EventArgs());
            }
            else
                GameWindow.Exit();
        }

        private void ResetCells()
        {
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    var currentCell = _cells[i, j];
                    currentCell.Reset();
                }
            }
        }
    }
}