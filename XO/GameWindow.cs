﻿using System;
using static SDL2.SDL;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace XOGame
{
    public abstract class GameWindow : IDisposable
    {
        private Stopwatch _gameTimer;
        private static bool _quit = false;
        private const float MsPerUpdate = 0.05f;

        private Window _window;
        public Window Window { get => _window; }
        private Renderer _renderer;
        public Renderer Renderer { get => _renderer; }

        public GameWindow(
            SDL_WindowFlags windowFlag, 
            uint systemFlag, 
            int width = 800, 
            int height = 600, 
            string title = "Window")
        {
            WindowInit(windowFlag, systemFlag, width, height, title);
            RenderInit();
            TimeInit();
        }

        public abstract void Initialize();
        public abstract void Update(float deltaTime);
        public abstract void Render(Renderer renderer);

        public void Run()
        {
            Initialize();

            float previous = GetCurrentTime();
            float lag = 0.0f;

            while (!_quit)
            {
                float current = GetCurrentTime();
                float elapsed = current - previous;
                previous = current;
                lag += elapsed;
                
                EventLoop();

                //while (lag >= MsPerUpdate)
                //{
                    Update(elapsed);
                //    lag -= MsPerUpdate;
                //}

                Render(_renderer);
            }
        }

       public static void Exit() =>
            _quit = true;

        public void Dispose()
        {
            _window.Dispose();
            _renderer.Dispose();
        }

        private void OnQuit(object sender, EventArgs e) => 
            _quit = true;

        private int GetCurrentTime() =>
            _gameTimer.Elapsed.Seconds;

        private void EventLoop()
        {
            while (SDL_PollEvent(out SDL_Event currentEvent) == 1)
            //if (SDL_WaitEvent(out SDL_Event currentEvent) == 1)
            {
                InputSystem(currentEvent);
                WindowSystem(currentEvent);
            }
        }

        private void WindowSystem(SDL_Event currentEvent) =>
            _window.Update(currentEvent);

        private void InputSystem(SDL_Event currentEvent) => 
            Input.Update(currentEvent);

        private void TimeInit() =>
            _gameTimer = Stopwatch.StartNew();

        private void RenderInit()
        {
            _renderer = new Renderer(_window.Handle);
            _renderer.Initialize();
        }

        private void WindowInit(SDL_WindowFlags windowFlag, uint systemFlag, int width, int height, string title)
        {
            _window = new Window(windowFlag, systemFlag, width, height, title);
            _window.Initialize();
            _window.Quit += OnQuit;
        }
    }
}
