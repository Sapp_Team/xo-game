﻿using System;
using System.Collections.Generic;
using System.Text;
using XOGame.Entities;
using XOGame.Scenes;

namespace XOGame
{
    public class MoveController: BaseEntity
    {
        private Queue<Action<float>> _queueMove;
        private Action<float>[] _actions;

        public MoveController(Queue<Action<float>> actions)
        {
            _queueMove = actions;

            _actions = _queueMove.ToArray();
        }

        public void OnClearQueue(object sender, EventArgs e)
        {
            _queueMove.Clear();
        }   
        
        public void OnDequeuePlayer(object sender, EventArgs e)
        {
            _queueMove.Enqueue(_queueMove.Dequeue());
        }

        public void OnReinitialize(object sender, EventArgs e)
        {
            _queueMove.Clear();
            foreach (var action in _actions)
                _queueMove.Enqueue(action);
        }

        public override void Initialize()
        {

        }

        public override void Update(float deltaTime)
        {
            var playerMove = _queueMove.Peek();
            playerMove(deltaTime);
        }

        public override void Render(Renderer render)
        {
            
        }
    }
}
