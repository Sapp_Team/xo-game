﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathF
{
    public struct Vector3
    {
        public float X;
        public float Y;
        public float Z;

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public float Length()
        {
            if (X == 0 && Y == 0 && Z == 0)
                return 0;

            return (float)Math.Sqrt(LengthSquare());
        }

        /// <summary>
        /// Длинна вектора в квадрате
        /// </summary>
        /// <returns></returns>
        public float LengthSquare() => (X * X) 
                                     + (Y * Y) 
                                     + (Z * Z);

        /// <summary>
        /// Вычислить вектор единичной длинны
        /// </summary>
        /// <returns></returns>
        public Vector3 Unit() => 
            this * (1 / Length());

       /// <summary>
       /// Умножение вектора на скаляр
       /// </summary>
       /// <param name="value">Множитель</param>
       /// <returns></returns>
        public Vector3 Scalar(float value) => 
            new Vector3(X * value, Y * value, Z * value);

        /// <summary>
        /// Проекция текущего вектора на заданный
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public Vector3 Proj(Vector3 vector) =>
            (Vector3.Dot(this, vector) * (1 / vector.LengthSquare()) * vector);
        

        public float ProjLength(Vector3 vector)
        {
            return Vector3.Dot(this, vector) * (1 / vector.Length());
        }

        public Vector3 Perp(Vector3 vector) => this - Proj(vector);

        /// <summary>
        /// Скалярное произведение векторов
        /// </summary>
        /// <param name="vectorL"></param>
        /// <param name="vectorR"></param>
        /// <returns></returns>
        public static float Dot(Vector3 vectorL, Vector3 vectorR)
        {
            return vectorL.X * vectorR.X 
                 + vectorL.Y * vectorR.Y 
                 + vectorL.Z * vectorR.Z;
        }

        public static Vector3 Cross(Vector3 vectorL, Vector3 vectorR)
        {
            return new Vector3(vectorL.Y * vectorL.Z - vectorL.Z * vectorR.Y,
                               vectorL.Z * vectorR.X - vectorL.X * vectorR.Z,
                               vectorL.X * vectorR.Y - vectorL.Y * vectorR.X);
        }

        /// <summary>
        /// Угол между заданными векторами
        /// </summary>
        /// <param name="vectorL"></param>
        /// <param name="vectorR"></param>
        /// <returns></returns>
        public static float Angle(Vector3 vectorL, Vector3 vectorR)
        {
            var cosAngle = Dot(vectorL, vectorR) / (vectorL.Length() * vectorR.Length());

            return (float)Math.Acos(cosAngle);
        }

        public static Vector3 operator -(Vector3 vector)
        {
            return new Vector3(-vector.X, -vector.Y, -vector.Z);
        }

        public static Vector3 operator +(Vector3 vectorL, Vector3 vectorR)
        {
            return new Vector3(vectorL.X + vectorR.X, 
                               vectorL.Y + vectorR.Y, 
                               vectorL.Z + vectorR.Z);
        }

        public static Vector3 operator -(Vector3 vectorL, Vector3 vectorR)
        {
            return new Vector3(vectorL.X - vectorR.X, 
                               vectorL.Y - vectorR.Y, 
                               vectorL.Z - vectorR.Z);
        }

        public static Vector3 operator *(Vector3 vector, float value)
        {
            return vector.Scalar(value);
        }

        public static Vector3 operator *(float value, Vector3 vector)
        {
            return vector.Scalar(value);
        }
    }
}
